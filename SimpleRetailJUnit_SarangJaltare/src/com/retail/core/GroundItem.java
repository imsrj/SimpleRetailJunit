/* GroundItem class extending Item class and override getShippingCost() method,compareTo() method from Comparable interface
 * All Items with Shipping method = Ground belongs to this class
*/ 
package com.retail.core;

public class GroundItem extends Item {

	public GroundItem() {
		super("GROUND");
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getShippingCost() {
		// TODO Auto-generated method stub
		 double cost;
		 cost = (this.weight * 2.5) ;
		// code for adding tariff. Please uncomment to see the output.
	       /* cost = cost + (0.03 * this.getPrice());*/
		 cost = Math.round(cost * 100.0)/100.0;
		 return cost;
	}

	@Override
	public int compareTo(Object compareItem) {
		// TODO Auto-generated method stub
		Item compare = (Item)compareItem;
		return new Long(this.getUpc()).compareTo(new Long(compare.getUpc()));
		
	}
}
