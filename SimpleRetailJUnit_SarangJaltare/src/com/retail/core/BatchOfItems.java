/*BatchOfItems class get the ArrayList from Main class and creates a Batch of Items inside constructor.
 * Also it sorts the Batch List items on the basis of UPC of each element.
 * generateBatchReport method generates the report as per requirements and print it on console.
 * */


package com.retail.core;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class BatchOfItems {
     List<Item> batch = new ArrayList<Item>();
     double totalBatchCost=0;
     
     public BatchOfItems(List listOfItems) {
		// TODO Auto-generated constructor stub
        this.batch = listOfItems;
   	    Collections.sort(batch);
    	 
	}
   
     
     public void generateBatchReport() {
    	 
 	
 		//System.out.println("this is display method");
 		 System.out.format("%-100sDate:%s", "****SHIPMENT REPORT****", LocalDateTime.now());
 	     System.out.format("\n\n%-30s%-50s%-30s%-30s%-30s%-30s", "UPC", "Description", "Price", "Weight", "Ship Method", "Shipping Cost");
 		 System.out.println("");
 		
 		 for(int i =0 ; i < batch.size(); i++) {
 			totalBatchCost = totalBatchCost + batch.get(i).getShippingCost();
 			System.out.format("%-30s%-50s%-30s%-30s%-30s%-30s", batch.get(i).getUpc(),batch.get(i).getDescription(),batch.get(i).getPrice()
 					,batch.get(i).getWeight(), batch.get(i).getshippingMethod(), batch.get(i).getShippingCost() );
 			
 			
 			System.out.println("");
 		}
 		System.out.println("");
 		System.out.format("%-30s%145s", "TOTAL SHIPPING COST:" , totalBatchCost);
     }
    
	/*public double getTotalBatchCost() {
		
		if(batch.size()==0) {
			return 0;
		}
		
        for(int i =0 ; i < batch.size(); i++) {
 			
 			totalBatchCost = totalBatchCost + batch.get(i).getShippingCost();
 		}
		return totalBatchCost;
	}*/

	
     
}
