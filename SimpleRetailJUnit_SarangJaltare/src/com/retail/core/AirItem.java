/* All Items with Shipping method = Air belongs to this class
 * Extends Item class and override getShippingCost() method, compareTo() method from Comparable interface
 * */
package com.retail.core;


public class AirItem extends Item {

	public AirItem() {
		super("AIR");
		// TODO Auto-generated constructor stub
	}

	@Override
	public double getShippingCost() {
		double cost;
		long secToLastDigit;
		long temp = (this.getUpc()/10);
		secToLastDigit=  temp%10;
	    cost = (this.weight * secToLastDigit);
        
        // code for adding tariff. Please uncomment to see the output.
       /* cost = cost + (0.03 * this.getPrice());*/
        cost = Math.round(cost * 100.0)/100.0;
		return cost;
		
	}
	
	@Override
	public int compareTo(Object compareItem) {
		// TODO Auto-generated method stub
		Item compare = (Item)compareItem;
		return new Long(this.getUpc()).compareTo(new Long(compare.getUpc()));
		
	}

}
